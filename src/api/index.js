/* eslint-disable prettier/prettier */
/*
包含 n个接口请求函数的模块
*/
import ajax from "./ajax";

// 分公司接口
export * from "./subCompanyApi";

const BASE_URL = "http://10.202.5.9:5123";

/**
 * 查询站点结构
 * @param {*} pramas
 */
export const reqStructure = pramas => ajax(BASE_URL + "/datacenter/structure", pramas);

/**
 * 查询报警事件
 * @param {*} pramas
 */
export const reqEvent = pramas => ajax(BASE_URL + "/datacenter/event", pramas);

/**
 * 事件统计 strict 版本，可用来统计违规事件数
 * @param {*} pramas
 */
export const reqStatistic = pramas => ajax(BASE_URL + "/datacenter/statistic", pramas);

/**
 * 历史拥挤指数
 * @param {*} params
 */
export const reqCIHistory = params => ajax(BASE_URL + "/datacenter/ci/history", params);

/**
 * 当前拥堵指数
 * @param {*} pramas
 */
export const reqCINow = pramas => ajax(BASE_URL + "/datacenter/ci/now", pramas);

/**
 * 商店进店人数统计
 * @param {*} pramas
 */
export const reqCustomerEnter = pramas => ajax(BASE_URL + "/datacenter/customer/enter", pramas);

/**
 * 加油区或商超区服务时间统计
 * @param {*} pramas
 */
export const reqServiceStatistic = pramas => ajax(BASE_URL + "/datacenter/service/statistic", pramas);

/**
 * 商超区或收银区违规率统计
 * @param {*} pramas
 */
export const reqViolateNow = pramas => ajax(BASE_URL + "/datacenter/violate/now", pramas);

/**
 * 历史违规率统计
 * @param {*} pramas
 */
export const reqViolateHistory = pramas => ajax(BASE_URL + "/datacenter/violate/history", pramas);

/**
 * 实时站长在站率
 * @param {*} pramas
 */
export const reqManagerOnlineNow = pramas => ajax(BASE_URL + "/datacenter/manager_online/now", pramas);

/**
 * 历史站长在站率
 * @param {*} pramas
 */
export const reqManagerOnlineHistory = pramas => ajax(BASE_URL + "/datacenter/manager_online/history", pramas);

/**
 * 当前服务时间全天占比
 * @param {*} pramas
 */
export const reqRefuelServiceNow = pramas => ajax(BASE_URL + "/datacenter/refuel_service/now", pramas);

/**
 * 历史服务时间全天占比
 * @param {*} pramas
 */
export const reqRefuelServiceHistory = pramas => ajax(BASE_URL + "/datacenter/refuel_service/history", pramas);

/**
 * 实时综合指数
 * @param {*} pramas
 */
export const reqCompositeIndexNow = pramas => ajax(BASE_URL + "/datacenter/composite_index/now", pramas);

/**
 * 历史综合指数
 * @param {*} pramas
 */
export const reqCompositeIndexHistory = pramas => ajax(BASE_URL + "/datacenter/composite_index/history", pramas);

/**
 * 查询流清单
 * @param {*} pramas
 */
export const reqStreamQuery = pramas => ajax(BASE_URL + "/datacenter/stream/query", pramas);

/**
 * 拉取流
 * @param {*} pramas
 */
export const reqStreamPull = pramas => ajax(BASE_URL + "/datacenter/stream/pull", pramas);
