import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home/Home.vue";
import Home2 from "./views/Home2";
import Monitor from "./views/Monitor/Monitor.vue";
import Events from "./views/Events/Events.vue";
import StationAnalysis from "./views/StationAnalysis";
import SubCompanyAnalysis from "./views/SubCompanyAnalysis";
import Test from "./views/test/test.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: StationAnalysis
    },
    {
      path: "/index/:station",
      name: "home",
      component: StationAnalysis
    },
    {
      path: "/home2",
      name: "home2",
      component: Home2
    },
    {
      path: "/monitor",
      name: "monitor",
      component: Monitor
    },
    {
      path: "/events",
      name: "events",
      component: Events
    },
    {
      path: "/sub-company-analysis/:companyName/:dateTime",
      name: "sub-company-analysis",
      component: SubCompanyAnalysis
    },
    {
      path: "/station-analysis",
      name: "station-analysis",
      component: StationAnalysis
    },
    {
      path: "/test",
      name: "test",
      component: Test
    }
  ]
});
