export default {
  subCompanyList: [
    { value: 'xian', label: '西安分公司', id: '49' },
    { value: 'baoji', label: '宝鸡分公司', id: '1' },
    { value: 'yanan', label: '延安分公司', id: '2' },
    { value: 'tongchuan', label: '铜川分公司', id: '3' },
    { value: 'weinan', label: '渭南分公司', id: '4' },
    { value: 'xianyang', label: '咸阳分公司', id: '5' },
    { value: 'yulin', label: '榆林分公司', id: '6' },
    { value: 'hanzhong', label: '汉中分公司', id: '7' },
    { value: 'ankang', label: '安康分公司', id: '8' },
    { value: 'shangluo', label: '商洛分公司', id: '9' }
  ],
  stationList: [
    { value: 'xiwan', label: '西万路' },
    { value: 'ximen', label: '西门' },
    { value: 'mingdemen', label: '明德门' },
    { value: 'zhuhong', label: '朱宏路' },
    { value: 'fengchengshilu', label: '凤城十路' },
    { value: 'caotan', label: '草滩路' }
  ],
  stationList2: {
    'xiwan': '西万',
    'ximen': '西门',
    'mingdemen': '明德门',
    'zhuhong': '朱宏路',
    'fengchengshilu': '凤城十路',
    'caotan': '草滩'
  },
  // 事件表格查询的时候一个对应关系
  stationEventList: [
    { value: 'all', label: '全部' },
    { value: 'xiwan', label: '西万路' },
    { value: 'ximen', label: '西门' },
    { value: 'mingdemen', label: '明德门' },
    { value: 'zhuhong', label: '朱宏路' },
    { value: 'fengchengshilu', label: '凤城十路' },
    { value: 'caotan', label: '草滩路' }
  ],
  // 卸油口事件列表
  unloadActionList: [
    { value: '0', label: '全部' },
    { value: '1', label: '卸油口开启' },
    { value: '2', label: '接油管操作' },
    { value: '3', label: '油罐车到达' },
    { value: '4', label: '油罐车离开' },
    { value: '5', label: '卸油口关闭' },
    { value: '6', label: '断开静电夹' },
    { value: '7', label: '连接静电夹' },
    { value: '8', label: '员工离开' },
    { value: '9', label: '存在灭火器' },
    { value: '10', label: '油井巡检' },
    { value: '11', label: '水溶法检测操作' },
    { value: '12', label: '卸油口巡检' },
    { value: '13', label: '已放置消防器材' },
    { value: '14', label: '水溶法检测操作' },
    { value: '15', label: '非法入侵' }
  ],
  // 财务室事件列表
  safeBoxActionList: [
    { value: '0', label: '全部' },
    { value: '1', label: '开启保险柜' },
    { value: '2', label: '关闭保险柜' },
    { value: '3', label: '财务室打开' },
    { value: '4', label: '开启钱箱' }
  ],
  // 卸油口事件列表
  oilActionList: [
    { value: '0', label: '全部' },
    { value: '1', label: '服务不规范' },
    { value: '2', label: '油井巡检' },
    { value: '3', label: '巡检' },
    { value: '4', label: '未引导' },
    { value: '5', label: '未回零' },
    { value: '6', label: '胶管乱摆放' }
  ],
  // 便利店事件列表
  checkoutActionList: [
    { value: '0', label: '全部' },
    { value: '1', label: '单手' },
    { value: '2', label: '双手接递' },
    { value: '3', label: '服务超时' }
  ],
  // 操作行为状态，只有合规和违规两种，但是违规分为 1，2，3 三个等级
  statusData: [
    "合规",
    "违规"
  ],
  levelList: {
    '0': '合规',
    '1': '轻度违规',
    '2': '中度违规',
    '3': '重度违规'
  },
  levelList2: [
    { value: '4', label: '全部' },
    { value: '0', label: '合规' },
    { value: '1', label: '轻度违规' },
    { value: '2', label: '中度违规' },
    { value: '3', label: '重度违规' }
  ],
  // 站点区域
  categoryData: [
    // "refuel_overview", // 加油区全景
    "refuel_side", // 加油区
    // "refuel", // 加油区
    "checkout", // 便利店
    "safebox", //财务室
    // "switching_room", // 配电房
    "unload" // 卸油口
  ],
  // 站点区域对应关系
  categoryData2: {
    "refuel_overview": "加油区全景",
    "refuel_side": "加油区",
    "checkout": "便利店",
    "safebox": "财务室",
    "unload": "卸油口"
    // "switching_room": "配电房",
  },
  actionsData: [
    // 卸油口
    "卸油口开启",
    "接油管操作",
    "油罐车到达",
    "油罐车离开",
    "卸油口关闭",
    "断开静电夹",
    "连接静电夹",
    "员工离开",
    "存在灭火器",
    "油井巡检", // 卸油口
    // "水溶法检测操作", // 卸油口
    "卸油口巡检",
    "已放置消防器材",
    "胶管乱摆放", //
    "非法入侵", // 卸油口

    // 财务室
    "开启保险柜",
    "关闭保险柜",
    "财务室打开",
    "开启钱箱",

    // 加油区
    "服务不规范",
    "油井巡检",
    "巡检",
    "未引导",
    "未回零",

    // 便利店
    "单手",
    "双手接递",
    "服务超时"
  ]
}