# 加油站现场智能监管系统

项目地址：

码云：https://gitee.com/fuyi501/dataVP.git

github：https://github.com/fuyi501/dataVP.git

## 如何使用

1. 安装 node

下载地址：https://nodejs.org/en/download/

```sh
node -v # 查看 node 版本号，版本需要 >10
```

node 版本管理可以使用 [nvm](https://github.com/nvm-sh/nvm)

2. 安装 vue

vue 官网：https://cn.vuejs.org/index.html

```sh
npm install vue -g
```

3. 版本管理工具 git

安装 git：https://www.git-scm.com/

3. 克隆并启动项目

```sh
git clone https://gitee.com/fuyi501/dataVP.git
cd dataVP
npm install # 安装依赖
npm run serve # 启动项目
```

## 开发工具

- [iview](https://www.iviewui.com/) 一套基于 Vue.js 的高质量 UI 组件库。

- [inMap](http://inmap.talkingdata.com/#/docs/v2/Install) 丰富的图层、更好的用户体验、大数据地理可视化库。

- [echarts](https://echarts.baidu.com/index.html) 一个使用 JavaScript 实现的开源可视化库。

- [Vue-ECharts](https://github.com/ecomfe/vue-echarts/blob/master/README.zh_CN.md) ECharts 的 Vue.js 组件。基于 ECharts v4.1.0+ 开发，依赖 Vue.js v2.2.6+。

- [flv.js](https://github.com/bilibili/flv.js) An HTML5 Flash Video (FLV) Player written in pure JavaScript without Flash. LONG LIVE FLV!

- [~~Vue-Video-Player~~](https://github.com/surmon-china/vue-video-player/) 适用于 Vue 的 video.js 播放器组件。

- [echarts 主题构建](https://echarts.baidu.com/theme-builder/) echarts 主题构建工具。

- [百度地图主题构建](http://lbsyun.baidu.com/customv2/index.html) 百度地图主题构建工具。

## 地图下钻案例

- https://github.com/flute/echarts3-chinese-map-drill-down

- https://jshare.com.cn/highmaps/FSSm7A
